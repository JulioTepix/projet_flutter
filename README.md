# Flutter Project by TEPIXTLE Julio

## Getting Started

This project is a starting point for a Flutter application.
- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

## Le projet

Ce projet a été fait avec une fichier .json de musique pour afficher ses données avec le même principe du projet de films vu en TD.

## Possibilités

Dans ce projet on peut:
- Lister les chansons qui sont dans le fichier .json et regarder l'info ainsi que jouer la chanson.
- ajouter une nouvelle chanson à la liste avec ses données et un lien optionnel pour pouvoir jouer la chanson sur la plateforme que l'utilisateur souhaite.
- Modifier les données de la chanson.
- Ajouter une nouvelle chanson via un formulaire.

Tout ce proces ne prend pas en compte l'image.