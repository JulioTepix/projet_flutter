import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:flutterfinalproject/ui/home.dart';

class Songs {
  Songs({
    required this.title,
    required this.artist,
    required this.genre,
    required this.year,
    required this.artwork,
    required this.url,
    required this.id,
  });
  late final String title;
  late final String artist;
  late final String genre;
  late final String year;
  late final String artwork;
  late final String url;
  late final int id;

  Songs.fromJson(Map<String, dynamic> json){
    title = json['title'];
    artist = json['artist'];
    genre = json['genre'];
    year = json['year'];
    artwork = json['artwork'];
    url = json['url'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['title'] = title;
    _data['artist'] = artist;
    _data['genre'] = genre;
    _data['year'] = year;
    _data['artwork'] = artwork;
    _data['url'] = url;
    _data['id'] = id;
    return _data;
  }

  static Future<String> _getSongsAsset() async{
    return await rootBundle.loadString('songs.json');
  }

  static Future<List<Songs>> getSongs() async{
    String jsonString = await _getSongsAsset();
    List<dynamic> jsonResponse = jsonDecode(jsonString)['songs'] as List;
    List<Songs> rep=[];
    jsonResponse.forEach((element) {rep.add(Songs.fromJson(element));});
    return rep;
  }

  String toString(){
    return title;
  }



  /*static newSong(title, artist, genre, year, url){
    return Songs(title:title, artist:artist, genre: genre, year:year, artwork:"img/default-artwork.png", url:url);
  }*/

}