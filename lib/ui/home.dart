import 'package:flutter/material.dart';
import 'package:flutterfinalproject/model/songs.dart';
import 'package:flutterfinalproject/ui/addSong.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutterfinalproject/ui/editSong.dart';

class SongsList extends StatefulWidget {
  @override
  _SongsListState createState() => _SongsListState();
}

class _SongsListState extends State<SongsList> {

  List<Songs> listOfSongs = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Musique pour aujourd'hui"),
        backgroundColor: Color.fromARGB(255, 33, 47, 238),
        actions: [
          Padding(
            padding: EdgeInsets.fromLTRB(0,0,50,0),
            child: IconButton(
              icon: Icon(Icons.file_download, color: Colors.white),
              onPressed: () {
                _downloadList();
              },
            ),
          ),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 116, 118, 244),
      body: ListView.builder(
        itemCount: listOfSongs.length, //nb elements de la liste
        itemBuilder: (BuildContext context, int index) {
          return Stack(children: [
            songPresentation(listOfSongs[index], context),
            Positioned(top: 10, child: artSong(listOfSongs[index].artwork)),
          ]);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
            _addSong(context, listOfSongs.length);
        },
        tooltip: 'Ajouter une chanson',
        child: Icon(Icons.add),
      ),
    );
  }

  Widget songPresentation(Songs song, BuildContext context) {
    return InkWell(
        child: Container(
          margin: EdgeInsets.only(left: 60),
          width: MediaQuery
              .of(context)
              .size
              .width,
          height: 120,
          child: Card(
            color: Colors.black38,
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0, left: 54.0),
              child: Padding(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //widgets se positionnent a partir de la gauche
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(song.title,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                  color: Colors.white)),
                        ),
                        Text(
                          "Année: ${song.year}",
                          style: mainStyle(),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("Artist: ${song.artist}", style: mainStyle()),
                        Text(song.genre, style: mainStyle()),
                        IconButton(
                          icon: Icon(Icons.edit, color: Colors.white),
                          onPressed: (){
                            _editSong(context, song);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.delete, color: Colors.white),
                          onPressed: (){
                            _deleteSong(song);
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        onTap: () {
          debugPrint("Songs : ${song.title}");
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SongDetails(
                        song: song,
                      ))); //Navigator permet de changer de route
        });
  }

  TextStyle mainStyle() {
    return TextStyle(fontSize: 15, color: Colors.grey);
  }

  Widget artSong(String url) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
            image: AssetImage(url ??
                'https://assets.stickpng.com/thumbs/5a4613e5d099a2ad03f9c995.png'),
            fit: BoxFit.cover),
      ),
    );
  }

  void _downloadList() {
    Songs.getSongs().then((m) {
      setState(() {
        if (m != null) {
          m.forEach((item) => listOfSongs.add(item));
        } else {
          listOfSongs = [];
        }
      });
    });
  }

  void _addSong(BuildContext context, int id) async{
    final newSong = await Navigator.push(context,
    MaterialPageRoute(builder: (context) => AddSong(id)));
    setState(() {
      listOfSongs.add(newSong);
    });
  }

  void _editSong(BuildContext context, Songs song) async{
    final editSong = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => EditSong(song)));
    setState(() {
      for(int i=0; i<listOfSongs.length; i++){
        if(listOfSongs[i].id == editSong.id){
          listOfSongs[i] = editSong;
          break;
        }
      }
    });
  }

  void _deleteSong(Songs song) {
    showDialog(
        context: context,
        builder: (buildcontext) {
          return AlertDialog(
            title: Text("Supprimer"),
            content: Text("Voulez vous supprimer cette chanson"),
            actions: <Widget>[
              ElevatedButton(
                child: Text("Oui", style: TextStyle(color: Colors.white),),
                onPressed: (){
                  listOfSongs.remove(song);
                  setState(() {});
                  Navigator.pop(context);
                  },
              ),
              ElevatedButton(
                child: Text("Non", style: TextStyle(color: Colors.black),),
                onPressed: (){ Navigator.of(context).pop(); },
              )
            ],
          );
        }
    );
  }
}

class SongDetails extends StatelessWidget {

  final Songs song;

  const SongDetails({Key? key, required this.song})
      : super(key: key); 
  //    : super(key: key); //parametres entre {} deviennent optionnels
  //key est utilise pour renseigner la position dans l'arbre.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${song.title} details"),
        backgroundColor: Colors.blueGrey.shade900,
      ),
      body: ListView(
        children: [
          DetailsHaut(image: song.artwork, url: Uri.parse(song.url)),
          DetailMilieu(song: song),
          HorizontalLine(),
          SongCast(song),
          HorizontalLine(),
          //DetailFin(song)
        ]
      ),
    );
  }
}

class DetailsHaut extends StatelessWidget {
  final String image;
  final Uri url;

  const DetailsHaut({Key? key, required this.image, required this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Stack(alignment: Alignment.center, children: [
          Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: 170,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(image), fit: BoxFit.cover)),
          ),
          IconButton(
            icon: Icon(Icons.play_circle_outline, color: Colors.red),
            onPressed: () {
              _playMusic(url);
            },
          ),
        ]),
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [Color(0x00f5f5f5), Color(0xfff5f5f5)],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter),

          ),
          height: 80,
        ),
      ],
    );
  }
}

Future<void> _playMusic(url) async {
  if (!await launchUrl(url)) {
    throw Exception('Could not launch $url');
  }
}

class DetailMilieu extends StatelessWidget {
  final Songs song;


  const DetailMilieu({Key? key, required this.song}) :super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        children: [
          SongAlbum(song.artwork),
          SizedBox(width: 16,),
          Expanded(
            child: DetailSong(song),
          )
        ],
      ),
    );
  }

}

class DetailSong extends StatelessWidget {
  final Songs song;


  DetailSong(this.song);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("${song.year} . ${song.artist}".toUpperCase(),
          style: TextStyle(
              fontWeight: FontWeight.w400,
              color: Colors.cyan
          ),),
        Text(song.title,
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 32),),
        Text.rich(TextSpan(
            style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
            children: <TextSpan>[
              TextSpan(
                  text: song.title
              ),

            ]))
      ],
    );
  }
}

class SongAlbum extends StatelessWidget {
  final String poster;

  SongAlbum(this.poster);

  @override
  Widget build(BuildContext context) {
    var borderRadius = BorderRadius.all(Radius.circular(10));
    return Card(
      child: ClipRRect(
          borderRadius: borderRadius,
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width / 4,
            height: 160,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(poster), fit: BoxFit.cover)),
          )
      ),
    );
  }


}


class SongCast extends StatelessWidget{
  final Songs song;


  SongCast(this.song);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          SongField(field:"Chanteur/euse",value:song.artist),
          SongField(field:"Année",value:song.year),
          SongField(field:"Genre",value: song.genre,)
        ],
      ),
    );
  }

}

class SongField extends StatelessWidget{
  final String field;
  final String value;


  const SongField({Key? key,required this.field, required this.value}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("$field : ",style: TextStyle(
          color: Colors.lightBlue,
          fontSize: 15,
          fontWeight: FontWeight.w300
        ),),
        Expanded(
          child: Text(value,style: TextStyle(
            color: Colors.black,fontSize: 15,fontWeight: FontWeight.w300
          ),),
        )
      ],
    );
  }
}

class HorizontalLine extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal:16.0,vertical:12),
      child: Container(
        height: 0.5,
        color: Colors.grey,
      ),
    );
  }

}
