import 'package:flutter/material.dart';
import '/model/songs.dart';

class AddSong extends StatefulWidget {
  int id;
  AddSong(this.id);
  @override
  SongForm createState() {
    return SongForm(id);
  }
}


class SongForm extends State<AddSong>{
  final title = TextEditingController();
  final artist = TextEditingController();
  final genre = TextEditingController();
  final year = TextEditingController();
  final url = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final int id;

  SongForm(this.id);

  @override
  void dispose() {
    title.dispose();
    artist.dispose();
    genre.dispose();
    year.dispose();
    url.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return  Scaffold(
      appBar: AppBar(
        title: Text("Ajouter une chanson"),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                controller: title,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Titre de la chanson',
                  labelText: 'Titre',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: artist,
                decoration: const InputDecoration(
                  icon: Icon(Icons.music_note_outlined),
                  hintText: 'Artist',
                  labelText: 'Artist',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: genre,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Genre',
                  labelText: 'Genre',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: year,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Année',
                  labelText: 'Année',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: url,
                maxLines: 2,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Url pour reproduire la chanson',
                  labelText: 'URL',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.pop(context,Songs(title:title.text, artist:artist.text, genre: genre.text, year:year.text, artwork:"img/default-artwork.png", url:url.text, id: id+1));
                    }
                  },
                  child: Text('Submit'),
                ),
              ),
            ],
          ),
        ),
        /*ElevatedButton(
          onPressed: (){
            Navigator.pop(context,Songs.newSong());
          },
          child: Text("Ajouter une chanson clear"),
        ),*/
      ),
    );
  }

}