import 'package:flutter/material.dart';
import '/model/songs.dart';

class EditSong extends StatefulWidget {
  Songs song;
  EditSong(this.song);
  @override
  SongFormEdit createState() {
    return SongFormEdit(song);
  }
}


class SongFormEdit extends State<EditSong>{
  final Songs song;

  SongFormEdit(this.song);

  @override
  Widget build(BuildContext context) {
    final title = TextEditingController(text: song.title);
    final artist = TextEditingController(text: song.artist);
    final genre = TextEditingController(text: song.genre);
    final year = TextEditingController(text: song.year);
    final url = TextEditingController(text: song.url);
    final _formKey = GlobalKey<FormState>();

    return  Scaffold(
      appBar: AppBar(
        title: Text("Editer ${song.title}"),
      ),
      body: Center(
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextFormField(
                controller: title,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Titre de la chanson',
                  labelText: 'Titre',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: artist,
                decoration: const InputDecoration(
                  icon: Icon(Icons.music_note_outlined),
                  hintText: 'Artist',
                  labelText: 'Artist',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: genre,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Genre',
                  labelText: 'Genre',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: year,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Année',
                  labelText: 'Année',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              TextFormField(
                controller: url,
                maxLines: 2,
                decoration: const InputDecoration(
                  icon: Icon(Icons.queue_music),
                  hintText: 'Url pour reproduire la chanson',
                  labelText: 'URL',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Veuillez remplir ce champs';
                  }
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.pop(context,Songs(title:title.text, artist:artist.text, genre: genre.text, year:year.text, artwork:song.artwork, url:url.text, id: song.id));
                    }
                  },
                  child: Text('Submit'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}